<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rules\File;

class ExportExcelRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        return [
            'exportbarang' => ['required', File::types(['xlsx', 'csv', 'ods'])->max(1024)]
        ];
    }

    public function messages(): array
    {
        return [
            'exportbarang.required' => 'File harus ada',
        ];
    }

    public function attributes(): array
    {
        return [
            'exportbarang' => 'File export'
        ];
    }
}
