<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\ExportExcelRequest;
use App\Http\Requests\StoreBarangRequest;
use App\Http\Requests\UpdateBarangRequest;
use App\Models\Barang;
use App\Models\Category;
use App\Models\Satuan;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Storage;
use OpenSpout\Common\Entity\Row;

class BarangController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $data = Barang::with(['category', 'satuan'])->orderBy('id', 'desc')->paginate(5);
        return response()->json($data, Response::HTTP_OK);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $kategori = Category::get(['id', 'name']);
        $satuan = Satuan::get(['id', 'name']);

        return response()->json(['kategori' => $kategori, 'satuan' => $satuan], Response::HTTP_OK);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreBarangRequest $request)
    {
        $validData = $request->validated();

        $imageName = null;
        if($validData['image'])
        {
            $file = $validData['image'];
            $imageName = $file->hashName();
            $file->move(public_path('images'), $imageName);
        }

        $barang = Barang::create([
            'category_id' => $validData['kategory'],
            'satuan_id' => $validData['satuan'],
            'kode_barang' => $validData['code'],
            'nama_barang' => $validData['name'],
            'gambar' => $imageName
        ]);
        
        return response()->json($barang, Response::HTTP_CREATED);
    }

    /**
     * Display the specified resource.
     */
    public function show(Barang $barang)
    {
        $dataBarang = Barang::with(['category', 'satuan'])->where('id', $barang->id)->first();
        return response()->json($dataBarang, Response::HTTP_OK);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateBarangRequest $request, Barang $barang)
    {
        $validData = $request->validated();
        $updateData = [];
        if($validData['image'])
        {
            if($barang->gambar)
            {
                $imageName = $barang->gambar;
                unlink(public_path('images/'.$imageName));
            }

            $file = $validData['image'];
            $imageName = $file->hashName();
            $file->move(public_path('images'), $imageName);
            $updateData['gambar'] = $imageName;
        }
        
        $updateData['category_id'] = $validData['kategory'];
        $updateData['satuan_id'] = $validData['satuan'];
        $updateData['kode_barang'] = $validData['code'];
        $updateData['nama_barang'] = $validData['name'];

        $barang->update($updateData);

        return response()->json($barang, Response::HTTP_OK);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Barang $barang)
    {
        if($barang->gambar)
        {
            $imageName = $barang->gambar;
            unlink(public_path('images/'.$imageName));
        }

        $barang->delete();
        return response()->json(null, Response::HTTP_NO_CONTENT);
    }

    public function exportExcel(ExportExcelRequest $exportExcelRequest)
    {
        $validData = $exportExcelRequest->validated();
        switch ($validData['exportbarang']->extension()) {
            case 'ods':
                $reader = new \OpenSpout\Reader\ODS\Reader();
                break;
            case 'csv':
                $reader = new \OpenSpout\Reader\CSV\Reader();
                break;
            
            default:
                $reader = new \OpenSpout\Reader\XLSX\Reader();
                break;
        }

        $reader->open($validData['exportbarang']);
        $data = [];
        foreach ($reader->getSheetIterator() as $sheet) {
            foreach ($sheet->getRowIterator() as $key => $row) {
                if($key === 1) //skip header
                {
                    continue;
                }
                $cells = $row->toArray();

                $category = Category::where('name', $cells[0])->first();
                $satuan = Satuan::where('name', $cells[1])->first();

                if($category != null && $satuan != null)
                {
                    $input['category_id'] = $category['id'];
                    $input['satuan_id'] = $satuan['id'];
                    $input['kode_barang'] = $cells[2];
                    $input['nama_barang'] = $cells[3];
                    $data[] = $input;
                }
            }
        }
        
        $reader->close();
        foreach(array_chunk($data, 100) as $item) {
            Barang::insert($item);
        }
        return response()->json('done', Response::HTTP_CREATED);
    }

    public function importExcel()
    {
        // $writer = new \OpenSpout\Writer\XLSX\Writer();
        // $writer->openToFile(public_path('excel/file.xlsx'));
        // $writer = new \OpenSpout\Writer\CSV\Writer();
        // $writer->openToFile(public_path('excel/file.csv'));
        $writer = new \OpenSpout\Writer\ODS\Writer();
        $writer->openToFile(public_path('excel/file.ods'));

        // make folder
        // use Illuminate\Support\Facades\File;
        // $imageName = time() . rand(1, 1000) . '.' . $image->extension();
        // $nameFolder = 'storage/galery/';
        // $folderTujuan = public_path($nameFolder);
        // if (!File::exists($folderTujuan)) {
        //     File::makeDirectory($folderTujuan, 0777, true, true);
        // }
        // $image->move($folderTujuan, $imageName);

        //header
        $values = ['Kode Barang', 'Nama Barang', 'Nama Kategory', 'Nama Satuan'];
        $rowFromValues = Row::fromValues($values);
        $writer->addRow($rowFromValues);

        $barangs = Barang::with(['category:id,name', 'satuan:id,name'])->orderBy('id', 'asc')->get();
        foreach($barangs as $barang)
        {
            $values = [$barang['kode_barang'], $barang['nama_barang'], $barang['category']['name'], $barang['satuan']['name']];
            $rowFromValues = Row::fromValues($values);
            $writer->addRow($rowFromValues);
        }
        $writer->close();
        $headers = [
            // 'Content-Type' => 'text/csv'
            // 'Content-Type' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'//excel
            'Content-Type' => 'application/vnd.oasis.opendocument.spreadsheet'//ods
        ];
        // return response()->download(public_path('excel/file.csv'), 'file.csv', $headers);
        // return response()->download(public_path('excel/file.xlsx'), 'file.xlsx', $headers);
        return response()->download(public_path('excel/file.ods'), 'file.ods', $headers);
    }
}
