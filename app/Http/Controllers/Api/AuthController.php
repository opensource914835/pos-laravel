<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\LoginRequest;
use App\Http\Requests\RegisterRequest;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    public function register(RegisterRequest $registerRequest)
    {
        try {
            $validData = $registerRequest->validated();

            $user = User::create([
                'name' => $validData['name'],
                'email' => $validData['email'],
                'password' => Hash::make($validData['password'])
            ]);

            // $token = $user->createToken('auth_token')->plainTextToken;

            return response()->json([
                'data' => $user,
                // 'token' => $token,
                // 'token_type' => 'Bearer',
                'is_admin' => "user"
            ], Response::HTTP_CREATED);

        } catch (\Throwable $th) {
            return response()->json(['message' => $th->getMessage()], Response::HTTP_CONFLICT);
        }
    }

    public function login(LoginRequest $authLoginRequest)
    {
        try {
            $validData = $authLoginRequest->validated();

            if(! auth()->attempt($validData))
            {
                return response()->json([
                    'errors' => ['email' => ['Email or password wrong']]
                ], Response::HTTP_UNPROCESSABLE_ENTITY);
            }

            $user = User::where('email', $validData['email'])->firstOrFail();

            // $token = $user->createToken('auth_token')->plainTextToken;

            return response()->json([
                'message' => 'Login success',
                // 'token' => $token,
                // 'token_type' => 'Bearer',
                'is_admin' => ($user->is_admin === 1) ? "admin" : "user"
            ], Response::HTTP_OK);

        } catch (\Throwable $th) {
            return response()->json(['message' => $th->getMessage()], Response::HTTP_CONFLICT);
        }
    }

    public function logout()
    {
        try {
            DB::table('sessions')->where('user_id', '=', auth()->user()->id)->delete();

            return response()->json([
                'message' => 'logout success'
            ], Response::HTTP_OK);
        } catch (\Throwable $th) {
            return response()->json(['message' => $th->getMessage()], Response::HTTP_CONFLICT);
        }
    }
}
