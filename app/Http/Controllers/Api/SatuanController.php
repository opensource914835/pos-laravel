<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreSatuanRequest;
use App\Http\Requests\UpdateSatuanRequest;
use App\Models\Satuan;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class SatuanController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        if($request->header('X-Header-Name') === "barang")
        {
            $data = Satuan::get(['id', 'name']);
            return response()->json($data, Response::HTTP_OK);
        }
        $data = Satuan::with(['user'])->where('user_id', auth()->user()->id)->latest()->paginate(5);

        return response()->json($data, Response::HTTP_OK);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreSatuanRequest $request)
    {
        $validData = $request->validated();
        $data = Satuan::create([
            'user_id' => auth()->user()->id,
            'name' => $validData['name']
        ]);

        return response()->json($data, Response::HTTP_CREATED);
    }

    /**
     * Display the specified resource.
     */
    public function show(Satuan $satuan)
    {
        return response()->json($satuan, Response::HTTP_OK);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateSatuanRequest $request, Satuan $satuan)
    {
        $validData = $request->validated();
        
        $satuan->update($validData);
        return response()->json($satuan, Response::HTTP_OK);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Satuan $satuan)
    {
        $satuan->delete();
        return response()->json(null, Response::HTTP_NO_CONTENT);
    }
}
