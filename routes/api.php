<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\AuthController;
use App\Http\Controllers\Api\BarangController;
use App\Http\Controllers\Api\CategoryController;
use App\Http\Controllers\Api\SatuanController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/


Route::middleware(['throttle:auth'])->group(function(){
    Route::post('/register', [AuthController::class, 'register']);
    Route::post('/login', [AuthController::class, 'login']);
});

Route::middleware(['auth:sanctum'])->group(function(){
    Route::post('/logout', [AuthController::class, 'logout']);
    Route::get('/user', function(Request $request){
        return $request->user();
    });
    
    Route::apiResource('category', CategoryController::class);
    Route::apiResource('satuan', SatuanController::class);
    Route::resource('barang', BarangController::class)->except(['edit']);
    Route::post('/barang/upload', [BarangController::class, 'exportExcel']);
    Route::post('/barang/importexcel', [BarangController::class, 'importExcel']);
});

Route::get('/woke', function() {
    return 'ada';
});
